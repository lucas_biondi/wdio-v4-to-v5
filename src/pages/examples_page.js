'use strict';

class ExamplesPage {

    clickLink(href){
        browser.findElement(`a[href='/${href}']`).click();
    }

}

module.exports = new ExamplesPage();