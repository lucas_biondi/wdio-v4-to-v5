'use strict';

class LoginPage {

    get usernameInput()  { return browser.findElement('#username'); }
    get passwordInput()  { return browser.findElement('#password'); }
    get loginButton()    { return browser.findElement("[type='submit'] .fa-sign-in"); }
    get errorLabel()     { return browser.findElement('.flash.error'); }

    setUsername(username){
        this.usernameInput.waitForDisplayed();
        this.usernameInput.setValue(username);
    }

    setPassword(username){
        this.passwordInput.waitForDisplayed();
        this.passwordInput.setValue(username);
    }

    clickLoginButton(){
        this.loginButton.waitForDisplayed();
        this.loginButton.click();
    }

    getErrorLabel(){
        this.errorLabel.waitForDisplayed();
        return this.errorLabel.getText().trim().replace(/\r?\n|\r/,"");
    }

    isLoginButtonPresent(){
        return this.loginButton.isDisplayed();
    }

}

module.exports = new LoginPage();